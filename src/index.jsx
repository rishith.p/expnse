import React from "react";
import ReactDOM from "react-dom";

import Expenses from "./components/expenses";

import "normalize.css";
import "antd/dist/antd.css";
import "./styles/index.scss";

ReactDOM.render(<Expenses />, document.getElementById("root"));
module?.hot?.accept();
