import moment from "moment";
import { DatePicker } from "antd";
import PropTypes from "prop-types";
import React, { useRef } from "react";

import Modal from "../../common/modal";

import "./styles/index.scss";

const AddExpenseModal = ({ onClose, onSubmitExpense }) => {
    let dateValue = moment().format("X");
    const priceInput = useRef();
    const expenseInput = useRef();
    const categoryInput = useRef();

    const onSubmit = () => {
        onSubmitExpense({
            expense: expenseInput.current.value,
            category: categoryInput.current.value,
            date: dateValue,
            price: priceInput.current.value,
        });
    };

    const onChangeDate = (date) => {
        dateValue = moment(date).format("X");
    };

    return (
        <Modal title="Add Expense" onClose={onClose} onSubmit={onSubmit}>
            <table className="expense-form">
                <tbody className="body">
                    <tr className="row">
                        <td className="label-cell">
                            <label className="input-label">Date :</label>
                        </td>
                        <td className="input-cell">
                            <DatePicker
                                className="date-input"
                                defaultValue={moment(dateValue, "X")}
                                onChange={onChangeDate}
                            />
                        </td>
                    </tr>
                    <tr className="row">
                        <td className="label-cell">
                            <label className="input-label">Expense :</label>
                        </td>
                        <td className="input-cell">
                            <input
                                type="text"
                                className="text-input"
                                ref={expenseInput}
                            />
                        </td>
                    </tr>
                    <tr className="row">
                        <td className="label-cell">
                            <label className="input-label">Category :</label>
                        </td>
                        <td className="input-cell">
                            <input
                                type="text"
                                className="text-input"
                                ref={categoryInput}
                            />
                        </td>
                    </tr>
                    <tr className="row">
                        <td className="label-cell">
                            <label className="input-label">Amount :</label>
                        </td>
                        <td className="input-cell">
                            <input
                                type="number"
                                className="text-input"
                                ref={priceInput}
                            />
                        </td>
                    </tr>
                </tbody>
            </table>
        </Modal>
    );
};

AddExpenseModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    onSubmitExpense: PropTypes.func.isRequired,
};

export default AddExpenseModal;
