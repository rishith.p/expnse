import React from "react";
import PropTypes from "prop-types";

import { formatCurrency, CURRENCIES } from "../../common/utils/currency";

import "./styles/index.scss";

const ExpenseTable = ({ expenses }) => (
    <div className="table-container">
        <table className="table">
            <thead className="head">
                <tr className="head-row">
                    <th>Expense</th>
                    <th>Category</th>
                    <th>Date</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody className="body">
                {React.Children.toArray(
                    expenses.map(({ expense, category, date, price }) => (
                        <tr className="row">
                            <td>{expense}</td>
                            <td>{category}</td>
                            <td>
                                {new Intl.DateTimeFormat("en-GB").format(
                                    new Date().valueOf(date)
                                )}
                            </td>
                            <td>{formatCurrency(price, CURRENCIES.INDIA)}</td>
                        </tr>
                    ))
                )}
            </tbody>
        </table>
    </div>
);

ExpenseTable.propTypes = {
    expenses: PropTypes.arrayOf(PropTypes.object),
};

ExpenseTable.defaultProps = {
    expenses: [],
};

export default ExpenseTable;
