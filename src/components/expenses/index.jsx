import React from "react";

import ExpenseTable from "./expense-table";
import AddExpenseModal from "./add-expense-modal";
import { formatCurrency, CURRENCIES } from "../common/utils/currency";

import "./styles/index.scss";

class Expenses extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showExpenseModal: false,
            expenses: [
                {
                    expense: "Internet",
                    category: "Monthly",
                    date: 1624042979,
                    price: 2000,
                },
                {
                    expense: "Chicken",
                    category: "Food",
                    date: 1624042979,
                    price: 200,
                },
            ],
        };
    }

    getButton = () => (
        <button
            type="button"
            className="button primary-button"
            onClick={this.openExpenseModal}
        >
            Add Expense
        </button>
    );

    computeTotalAmount = (expenses) =>
        expenses.reduce(
            (accumulator, expense) => accumulator + expense.price,
            0
        );

    getTotalComponent = () => {
        const { expenses } = this.state;

        return (
            <div className="total-container">
                <span className="total-label">Total:</span>
                <span className="total-value">
                    {formatCurrency(
                        this.computeTotalAmount(expenses),
                        CURRENCIES.INDIA
                    )}
                </span>
            </div>
        );
    };

    closeExpenseModal = () => this.setState({ showExpenseModal: false });

    openExpenseModal = () => this.setState({ showExpenseModal: true });

    onSubmitExpense = (newExpense) => {
        this.setState((prev) => ({
            showExpenseModal: false,
            expenses: [...prev.expenses, newExpense],
        }));
    };

    render() {
        const { showExpenseModal, expenses } = this.state;

        return (
            <div className="expense-container">
                <div className="header-container">
                    {this.getTotalComponent()}
                    {this.getButton()}
                </div>
                <ExpenseTable expenses={expenses} />

                {showExpenseModal && (
                    <AddExpenseModal
                        onClose={this.closeExpenseModal}
                        onSubmitExpense={this.onSubmitExpense}
                    />
                )}
            </div>
        );
    }
}

export default Expenses;
