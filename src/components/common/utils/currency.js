const CURRENCIES = {
    INDIA: "INR",
};

const formatCurrency = (value, currency) =>
    new Intl.NumberFormat("en-US", {
        style: "currency",
        currency,
    }).format(value);

export { CURRENCIES, formatCurrency };
