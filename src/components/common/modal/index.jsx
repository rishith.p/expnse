import React from "react";
import PropTypes from "prop-types";

import "./styles/modal.scss";

const Modal = ({ children, onClose, onSubmit, title }) => (
    <div className="modal-mask">
        <div className="modal-dialog">
            <div className="modal-header">
                <span className="title">{title}</span>
                <span
                    label="Close Modal"
                    className="close-button"
                    onClick={() => onClose()}
                    role="button"
                    tabIndex={0}
                />
            </div>
            <div className="modal-content">{children}</div>
            <div className="modal-footer">
                <button
                    type="button"
                    className="button secondary-button"
                    onClick={onClose}
                >
                    Cancel
                </button>
                <button
                    type="submit"
                    className="button primary-button modal-primary-button"
                    onClick={onSubmit}
                >
                    Submit
                </button>
            </div>
        </div>
    </div>
);

Modal.defaultProps = {
    title: "",
};

Modal.propTypes = {
    children: PropTypes.element.isRequired,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
};

export default Modal;
